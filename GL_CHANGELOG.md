# GitLab fork Changelog

This documents a list of cohesive changes made for the GitLab fork of [vscode](https://github.com/microsoft/vscode).

## 1.69.1-1.0.0

- Fork initialization and documentation.
  - https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/12
  - Initialized this fork ontop of [upstream tag 1.69.1](https://github.com/microsoft/vscode/tree/1.69.1)
  - Added files:
    - `GL_CHANGELOG.md`
    - `GL_CONTRIBUTING.md`
    - `GL_FORK_PROCESS.md`
  - Updated `README.md`
  - Updated `.vscode/settings.json`
    - We don't need `alwaysCommitToNewBranch` and branch protections..
